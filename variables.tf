
variable "region" {
  type = string
  description = "The region in which to create/manage resources"
  default = "us-east-2"
}

# export TF_VAR_account_id=your-account-number
variable "account_id"{
  type        = string
  description = "The account ID in which to create/manage resources"
}
